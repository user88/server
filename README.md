



L’application démarre un serveur et écoute le port 3000 à la recherche de connexions. L’application répond “Hello World!” aux demandes adressées à l’URL racine (/) ou à la route racine. Pour tous les autres chemins d’accès, elle répondra par 404 Not Found.